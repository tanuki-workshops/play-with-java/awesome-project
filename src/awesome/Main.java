package awesome;

import java.lang.String;
import java.lang.System;
import acme.FourtyTwo;
import wonderland.MagicNumber;

public class Main {

    public static void main( String[] args )
    {
        System.out.println("👋 hello world 🌍");
        FourtyTwo myNumber = new FourtyTwo();
        System.out.println(myNumber.fullySpelledOut());

        System.out.println((new MagicNumber()).fullySpelledOut());

    }

}
